<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class FlashCard extends Model
{
    protected $fillable = ['word', 'pronunciation', 'meaning', 'language'];

    public static $languages = ["English" => "English", "Japanese" => "Japanese", "Vietnamese" => "Vietnamese"];

    /*
     * get param to return available language
     * @param $language:string
     * @return string
     * */
    public static function getByLanguage($language = NULL)
    {
        switch (strtolower($language)) {
            case 'en':
            case 'eng':
            case 'english':
                return self::where('language', 'English')->get();
            case 'vn':
            case 'viet':
            case 'vietnam':
                return self::where('language', 'Vietnamese')->get();
            case 'ja':
            case 'jp':
            case 'jap':
            case 'japanese':
                return self::where('language', 'Japanese')->get();
            default:
                return self::all();
        }
    }
}
