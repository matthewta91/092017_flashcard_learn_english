<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Model::unguard();

        $this->call('FlashCardTableSeeder');
        $this->command->info('Flash cards seeded.');
    }
}

class FlashCardTableSeeder extends Seeder {

    public function run()
    {
        DB::table('flash_cards')->delete();

        App\FlashCard::create([
            'word' => 'おはよう',
            'meaning' => 'good morning',
            'pronunciation' => 'ohayou',
            'language' => 'Japanese'
        ]);
    }

}
