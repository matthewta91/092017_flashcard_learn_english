<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFlashCardsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('flash_cards', function(Blueprint $table)
        {
        $table->increments('id');
        $table->string('word'); /*tu can ghi nho*/
        $table->string('meaning'); /*y nghia cua tu*/
        $table->string('pronunciation'); /*cach phat am*/
        $table->string('language'); /*ngon ngu*/
        $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('flash_cards');
    }
}
