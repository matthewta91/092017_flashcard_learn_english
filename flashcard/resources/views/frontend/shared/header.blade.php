<nav class="navbar">
    <div class="container">
        <div id="logo">
            <a href="{!! action('Frontend\FlashCardController@index') !!}"> Flash Cards </a>
        </div>
        <div class="navbar-search">
            <div class="btn-login">
                <a href="{!! action('Frontend\FlashCardController@create') !!}" > Create </a>
            </div>
        </div>
    </div>
</nav>
<nav class="navbar-main">
    <div class="container">
        <div class="cat-title">Category</div>
        <ul class="list-menu">
            <li class="all category-active">
                <a href="{!! action('Frontend\FlashCardController@index')!!}" > All </a>
            </li>
            <li class="en category-active">
                <a href="{!! action('Frontend\FlashCardController@index', ['lang' => 'en']) !!}"> English </a>
            </li>
            <li class="ja category-active">
                <a href="{!! action('Frontend\FlashCardController@index', ['lang' => 'ja']) !!}"> Japanese </a>
            </li>
            <li class="vn category-active">
                <a href="{!! action('Frontend\FlashCardController@index', ['lang' => 'vn']) !!}"> Vietnamese </a>
            </li>
        </ul>
    </div>
</nav>
