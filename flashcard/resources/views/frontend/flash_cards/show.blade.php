@extends('frontend.layouts.default')

@section('jumbotron')
    <div class="jumbotron">
        <div class="container">
            <h1>{{ htmlspecialchars($flashCard->word, ENT_QUOTES) }}</h1>
            <p class="pronunciation">{{ htmlspecialchars($flashCard->pronunciation, ENT_QUOTES) }}</p>
            <p class="meaning">{{ htmlspecialchars($flashCard->meaning, ENT_QUOTES) }}</p>
        </div>
    </div>
@endsection

@section('content')
    <div class="row">
        <div class="container">
            <div class="box-title">
                <h2>
                    <a>Other flash cards</a>
                </h2>
            </div>
        </div>
        @include('frontend.flash_cards.list')
    </div>
@endsection
